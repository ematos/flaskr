import sqlite3
from mock import patch
from .. import flaskr
import unittest
from unipath import Path


def mock_db():
    schema_file = Path(__file__).parent.parent.child('schema.sql')
    with open(schema_file) as f: schema_sql = f.read()
    
    conn = sqlite3.connect(':memory:')
    cursor = conn.cursor()
    cursor.executescript(schema_sql)
    conn.commit()

    return conn


@patch.object(flaskr, 'connect_db', mock_db)
class ShowEntriesTest(unittest.TestCase):
    def setUp(self):
        flaskr.app.config['TESTING'] = True
        self.app = flaskr.app.test_client()

    def test_get_login(self):
        content = self.app.get('/login').data.decode('utf-8')
        assert '<form' in content
        assert 'name="username"' in content
        assert 'name="password"' in content
        assert '<button>' in content

    def test_post_correct_login(self):
        with flaskr.app.test_client() as app:
            content = app.post('/login', data={'username': 'admin', 'password': 'default'},
                               follow_redirects=True).data.decode('utf-8')

            assert 'You were logged in!' in content
            assert flaskr.session['logged_in']

    def test_post_wrong_login(self):
        content_wrong_username = self.app.post('/login', data={'username': 'omg', 'password': 'default'}).data.decode('utf-8')
        content_wrong_password = self.app.post('/login', data={'username': 'admin', 'password': 'omg'}).data.decode('utf-8')
        content_both_wrong = self.app.post('/login', data={'username': 'ham', 'password': 'egg'}).data.decode('utf-8')

        assert '<strong>Error: </strong> Invalid username or password' in content_wrong_username
        assert '<strong>Error: </strong> Invalid username or password' in content_wrong_password
        assert '<strong>Error: </strong> Invalid username or password' in content_both_wrong

    def test_logout(self):
        with flaskr.app.test_client() as app:
            resp = app.get('/logout', follow_redirects=True)
            assert 'You were logged out!' in resp.data.decode('utf-8')
            assert not flaskr.session.get('logged_in')


if __name__ == '__main__':
    unittest.main()
